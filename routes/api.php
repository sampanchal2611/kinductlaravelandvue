<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlayersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*Upload Json File and Store All Details  */
Route::post('uploadplayers',[PlayersController::class,'store']);
/*Get All Player and Details  */
Route::get('playerlist',[PlayersController::class,'index']);
/*When pass id get Particulare Player Details */
Route::get('playerdetail/{id}',[PlayersController::class,'show']);
/*Deleted Player and Details  */
Route::delete('player/{id}',[PlayersController::class,'destroy']);
