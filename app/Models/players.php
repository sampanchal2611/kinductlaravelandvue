<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class players extends Model
{
    use HasFactory;
    public $table = 'players';
    
    protected $fillable = [
        'name',
        'age',
        'location',
    ];
}
