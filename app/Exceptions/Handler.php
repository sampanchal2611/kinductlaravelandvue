<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use App\Exceptions\CustomException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
       $this->renderable(function (Throwable $e) {

            if ($e instanceof NotFoundHttpException) {
                return response()->json(
                                ['errors' => [
                                'status' => 401,
                                'message' => 'Something went Wrong',
                            ]
                                ], 401
                );
            }
            if ($e instanceof ModelNotFoundException) {
                return response()->json(
                                ['errors' => [
                                'status' => 404,
                                'message' => 'Something went Wrong',
                            ]
                                ], 404
                );
            }
            if ($e instanceof \Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException) {
            return response()->json(
                                            ['errors' => [
                                            'status' => 503,
                                           'message' => 'Something went Wrong in Method',
                                        ]
                                            ], 503
                 );
                }
        });
    }
}
