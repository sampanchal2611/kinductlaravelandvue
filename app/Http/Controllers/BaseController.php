<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    //make error ans suess handale 
    public static  function sendResponse($result,$message){
        
        $response =[
            "success" => true,
            "data"   => $result,
            "message"=>$message,
        ];
        
        return response()->json($response,200);
        
    }
    
    public static function sendError($error,$errorMessages =[])
    {
        $response =[
            "success" =>false,
            "message"=>$error,
        ];
        if(!empty($errorMessages))
        {
            $response['data']=$errorMessages;
        }
        return response()->json($response);
    }
    
}
