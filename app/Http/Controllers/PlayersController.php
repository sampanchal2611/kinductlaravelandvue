<?php

namespace App\Http\Controllers;

use App\Models\players;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\CreatePlayersRequest;

class PlayersController extends Controller {

    /**
     * Display ALL players Details listing.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $responce = players::all()->toArray();
        return BaseController::sendResponse($responce, 'Players Detail retrieved successfully.');
    }
    
    

    /**
     * @Description: FIle Upload and Store Player Details.
     *
     * @param   $request  json file
     * @return  Response Success or Failure
     */
    public function store(Request $request) {

        if ($request->hasFile('file')) {
            //Get file Exteson and check
            $file = $request->file->getClientOriginalExtension();
            if ($file == 'json') {
                $playerDetail = json_decode(file_get_contents($request->file), true);
                $data = [];
                foreach ($playerDetail['Players'] as $val) {
                    $data[] = ([
                        'name' => $val['Name'],
                        'age' => $val['Age'],
                        'location' => json_encode($val['Location']),
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')]);
                }
                $result = players::insert($data);
                
                if (!empty($result)) {
                    //File Save in Pulic Folder
                    $this->saveFile($request->file);
                    return BaseController::sendResponse($data, 'Player Detail Added successfully.');
                }
            } else {
                return BaseController::sendError('Its Only Allow Json File');
            }
        } else {
            return BaseController::sendError('Please File Upload Poperly');
        }
    }

    /**
     * @Description: Fine Particular  Player Details  
     *
     * @param  $players Id
     * @return Response  Success or Failure
     */
    public function show($id = array()) {
        $playerDetail = players::findOrFail($id)->toArray();
          if (is_null($playerDetail)) {
            return BaseController::sendError('UserDetail not found.');
        }
        return BaseController::sendResponse($playerDetail, 'UserDetail retrieved successfully.');
    }

    /**
     * @Description: Remove the specified Player Details via ID from storage.
     *
     * @param   $players ID 
     * @return Response  Success or Failure
     */
    public function destroy(Request $request) {
        $playerDetail = players::findOrFail($request->id);
        if ($playerDetail->delete()) {
            return BaseController::sendResponse($playerDetail, 'UserDetail Deleted successfully.');
        }
    }
     public function saveFile($images =array()){
                $file = $images->getClientOriginalName();
                $filename = pathinfo($file, PATHINFO_FILENAME);
                $extension = pathinfo($file, PATHINFO_EXTENSION);
                $imagename= $filename .'_'.time().'_' . '.' . $extension;  
                $path= $images->move(public_path() . '/Files/', $imagename);
               return $imagename;                 
    }

}
