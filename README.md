## Assessment Instructions

In this assessment, you will be creating a simple `Player dashboard` using the `VueJS` front-end component library and the `Laravel Framework`.
Images here Blow Link
`https://kinduct.invisionapp.com/console/share/ZSRFJQ5KC3Q/356898365#%2Fscreens`

## Requirements

 user input JSON file  and store the contents in a database Name IS `kinduct` of  `MySQL` ,With `LARAVEL END POINT`

## Fields

The following fields are expected to be captured and stored: `Name`, `AGE` & `LOCATION`.


## Project Setup

Before start working you should copy the content of `.env.example` file into `.env` file and run the following commands.

### Downloads and install all `PHP` and  dependencies
```
composer install
```

### Downloads and install all `JavaScript` dependencies
```
npm install
```

### Generates `Laravel` application 

```
php artisan key: generate

`php artisan migrate`
```

### Spins up a development server and host your application
```
php artisan serve OR npm run watch
```

After that, you should be able to access the project on your `http://127.0.0.1:8000/`.