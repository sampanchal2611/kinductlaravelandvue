import VueRouter from 'vue-router';
import Vue from 'vue';
require('./bootstrap');

Vue.use(VueRouter);
//Set all component vue Path
const dashboard = require('./components/dashboard.vue').default;
const athlete = require('./components/athlete.vue').default;
const athleteDetail = require('./components/athlete-detail.vue').default;
const routes = [

    {
        //upoad file component
        path: '/',
        component: dashboard
    },
    {
        //Dispaly all player detail
        path: '/athlete-list',
        component: athlete,
        name: 'athlete-list'
    },
    {   //Dispaly specific player detail
        path: '/athlete-detail/:id',
        component: athleteDetail
    }
];

const router = new VueRouter({
    routes

});

const app = new Vue({
    el: '#app',
    router

});
