import Vue from 'vue';
import Router from 'vue-router';

import dashboard from './components/dashboard.vue';
import athlete from './components/athlete.vue';


export default new Router({
    mode: 'hitory ',
    routes: [
        {path: '/',
            component: dashboard
        },
        {
            path: '/athlete', component: athlete
        }
    ]
})