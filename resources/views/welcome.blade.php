@extends('layout.app')
@section('content')
<header class="header"></header>
<div class="navigation">
    <nav class="nav" role="navigation">
       <a><router-link to="/athlete-list">Athlete</router-link></a>
       <a><router-link to="/">Upload Athlete</router-link></a>
    </nav> 
</div>
<hr>
<div>
    <router-view></router-view>
</div>
@endsection